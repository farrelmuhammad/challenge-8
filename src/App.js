import logo from './LinkedIn_icon.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hello, I'm <code>Farrel</code>
        </p>
        <a
          className="App-link"
          href="https://linkedin.com/in/farrelmuhammad"
          target="_blank"
          rel="noopener noreferrer"
        >
          Linkedin
        </a>
      </header>
    </div>
  );
}

export default App;
